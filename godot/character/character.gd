extends KinematicBody

export var speed = 10
export var acceleration = 5
export var gravity = 0.98
export var jump_power = 30
export var mouse_sensitivity = 0.3
export var inverse_camera_x:bool = false
export var inverse_camera_y:bool = false
export var use_spot:bool = true

onready var head = $Head
onready var camera = $Head/Camera

var velocity = Vector3()
var camera_x_rotation = 0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	if !use_spot:
		var s:Light = $Head/Camera/spot
		s.get_parent().remove_child( s )
		s.queue_free()

func _input(event):
	if event is InputEventMouseMotion:
		var mx:float = 1
		var my:float = 1
		if inverse_camera_x:
			mx = -1
		if inverse_camera_y:
			my = -1
		head.rotate_y(deg2rad(-event.relative.x * mx * mouse_sensitivity))
		var x_delta = event.relative.y * my * mouse_sensitivity
		if camera_x_rotation + x_delta > -90 and camera_x_rotation + x_delta < 90: 
			camera.rotate_x(deg2rad(-x_delta))
			camera_x_rotation += x_delta

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _physics_process(delta):
	var head_basis = head.get_global_transform().basis
	
	var direction = Vector3()
	if Input.is_action_pressed("move_forward"):
		direction += head_basis.z
	elif Input.is_action_pressed("move_backward"):
		direction -= head_basis.z
	
	if Input.is_action_pressed("move_left"):
		direction += head_basis.x
	elif Input.is_action_pressed("move_right"):
		direction -= head_basis.x
	
	direction = direction.normalized()
	
	velocity = velocity.linear_interpolate(direction * speed, acceleration * delta)
	velocity.y -= gravity
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_power
	
	velocity = move_and_slide(velocity, Vector3.UP)
