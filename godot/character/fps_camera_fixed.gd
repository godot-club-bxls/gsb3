extends Spatial

export (float,0,10) var view_sensitivity : float = 0.25
export (float,0,10) var rotation_damping : float = 5
export (float,0,10) var acceleration_speed : float = 0.2
export (float,0,10) var decay_speed : float = 4
export (float,0,10) var max_speed : float = 8

var yaw : float = 0
var pitch : float = 0
var directions : Array = [false,false,false,false] # UP, RIGHT, DOWN, LEFT
var momentum : Vector3 = Vector3()

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	if event is InputEventMouseMotion:
		yaw = (fmod(yaw - event.relative.x * view_sensitivity, 360))
		pitch = max(min(pitch - event.relative.y * view_sensitivity, 85), -85)

func _process(delta):
	var decay : float = min(1.0, decay_speed * delta)
	momentum -= momentum * decay
	if directions[0]:
		momentum.z -= acceleration_speed * delta
	if directions[2]:
		momentum.z += acceleration_speed * delta
	if directions[1]:
		momentum.x += acceleration_speed * delta
	if directions[3]:
		momentum.x -= acceleration_speed * delta
	var l : float = momentum.length()
	if l > 0 and delta > 0 and l / delta > max_speed:
		momentum = momentum.normalized() * max_speed * delta
	#self.translation += $cam.global_transform.basis.xform(momentum)
	var b0 : Basis = Basis( Vector3.UP, rotation.y )
	var b1 : Basis = Basis( Vector3.UP, deg2rad(yaw) )
	self.transform.basis = b0.slerp( b1, min(1.0,rotation_damping*delta) )
	#b0 = Basis( Vector3.RIGHT, $cam.rotation.x )
	#1 = Basis( Vector3.RIGHT, deg2rad(pitch) )
	#$cam.transform.basis = b0.slerp( b1, min(1.0,rotation_damping*delta) )
