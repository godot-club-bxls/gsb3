tool

extends MeshInstance

export (bool) var _synch_materials : bool = true
export (float,0,10) var _speed : float = 1
export (float,-5,5) var _curve_x : float = 0 setget curve_x
export (float,-5,5) var _curve_y : float = 0 setget curve_y
export (float,0,5) var _multiply : float = 1 setget multiply
export (NodePath) var _gaze : NodePath = "" setget gaze



var material_body : ShaderMaterial = null
var material_eye : ShaderMaterial = null
var eyes_center : Vector3 = Vector3(0,0.039,0.282)
var gaze_node : Spatial = null
var gaze_current : Vector3 = Vector3()
var gaze_target : Vector3 = Vector3()
var timer : float = 0

func curve_x( f ):
	_curve_x = f
	update_curvature()

func curve_y( f ):
	_curve_x = f
	update_curvature()

func update_curvature():
	if material_body == null:
		return
	if abs(_curve_x) < 1e-5:
		_curve_x = 0
	if abs(_curve_y) < 1e-5:
		_curve_y = 0
	var curv = Vector3( _curve_x, _curve_y, 0.0 )
	material_body.set_shader_param( "curvature", curv )
	material_eye.set_shader_param( "curvature", curv )

func multiply( f ):
	_multiply = f
	if material_body == null:
		return
	material_body.set_shader_param( "factor_mult", _multiply )
	material_eye.set_shader_param( "factor_mult", _multiply )

func gaze( np: NodePath ):
	_gaze = np
	if material_body == null:
		return
	if np == "":
		gaze_node = null
		return
	gaze_node = get_node( np )
	if gaze_node != null and not gaze_node is Spatial:
		gaze_node = null

func init():
	material_body = self.mesh.surface_get_material(0)
	material_eye = self.mesh.surface_get_material(1)
	if material_body == null or material_eye == null:
		material_body = null
		material_eye = null
		return
	if _synch_materials:
		# setting vars
		update_curvature()
		multiply( _multiply )
		gaze( _gaze )
		# reseting gaze
		gaze_current = Vector3()
		gaze_target = Vector3()
		material_eye.set_shader_param( "uv1_offset", gaze_current )
		# synching mats
		material_eye.set_shader_param( "curvature", material_body.get_shader_param( "curvature" ) )
		material_eye.set_shader_param( "curve_size", material_body.get_shader_param( "curve_size" ) )
		material_eye.set_shader_param( "curve_offset", material_body.get_shader_param( "curve_offset" ) )
		material_eye.set_shader_param( "curve_accel", material_body.get_shader_param( "curve_accel" ) )
		material_eye.set_shader_param( "timer", material_body.get_shader_param( "timer" ) )
		material_eye.set_shader_param( "swim_mult", material_body.get_shader_param( "swim_mult" ) )
		material_eye.set_shader_param( "factor_mult", material_body.get_shader_param( "factor_mult" ) )
		material_eye.set_shader_param( "twist_factor", material_body.get_shader_param( "twist_factor" ) )
		material_eye.set_shader_param( "s_factor", material_body.get_shader_param( "s_factor" ) )
		material_eye.set_shader_param( "yaw_factor", material_body.get_shader_param( "yaw_factor" ) )
		material_eye.set_shader_param( "head_z", material_body.get_shader_param( "head_z" ) )
		material_eye.set_shader_param( "head_color", material_body.get_shader_param( "head_color" ) )
		material_eye.set_shader_param( "tail_z", material_body.get_shader_param( "tail_z" ) )
		material_eye.set_shader_param( "tail_color", material_body.get_shader_param( "tail_color" ) )

func _ready():
	init()

func _process(delta):
	
	if material_body == null:
		init()
	if material_body == null:
		return
	
	# animation
	timer += _speed * delta
	material_body.set_shader_param( "timer", timer )
	material_eye.set_shader_param( "timer", timer )
	
	# gaze
	if gaze_node != null:
		var gp : Vector3 = ( to_local( gaze_node.global_transform.origin ) - eyes_center ).normalized()
		var gxz : Vector2 = Vector2( gp.x, gp.z ).normalized()
		var ga : float = gxz.angle()
		var gx : float = (1.0-sin(ga))*.15
		if gxz.x < 0:
			gx *= -1
		gaze_target = Vector3(gx,gp.y*.3,0) 
		gaze_current += ( gaze_target - gaze_current ) * min(1.0,8.0*delta)
		material_eye.set_shader_param( "uv1_offset", gaze_current )
