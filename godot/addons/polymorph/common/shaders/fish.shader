// fish shader
// purpose: vertex animation of mesh with multiple controls over deformation facttors
// upside: precise, shader recompute local position & normal for each vertex
// downside: process demanding
// author: frankiezate
// url: http://polymorph.cool
// date: 2020.07.09
// license: MIT

shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

// curvature
uniform vec3 curvature;
uniform vec3 curve_size;
uniform vec3 curve_offset;
uniform vec3 curve_accel;

// swimming
uniform float timer;
uniform float swim_mult;
uniform float factor_mult;
uniform float twist_factor;
uniform float s_factor;
uniform float yaw_factor;
uniform float head_z;
uniform vec4 head_color;
uniform float tail_z;
uniform vec4 tail_color;

uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform sampler2D texture_normal : hint_normal;
uniform float normal_scale : hint_range(-16,16);
uniform sampler2D texture_ambient_occlusion : hint_white;
uniform vec4 ao_texture_channel;
uniform float ao_light_affect;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

void vertex() {
	
	mat3 normrot = mat3( vec3(1.0,0.0,0.0), vec3(0.0,1.0,0.0), vec3(0.0,0.0,1.0) );
	
	//////////////
	// SWIMMING //
	//////////////
	
	// control
	float dist_ht = head_z - tail_z;
	float ranged_z = max( 0.0, min( 1.0, ( VERTEX.z - head_z ) / dist_ht ) );
	vec4 ctrl_color = tail_color * ranged_z + head_color * (1.0-ranged_z);
	float swim_angl = sin( ( VERTEX.z + timer ) * swim_mult ) * factor_mult;
	
	// twist
	float twist_a = ctrl_color.r * swim_angl * twist_factor;
	float twist_cos = cos( twist_a );
	float twist_sin = sin( twist_a );
	vec3 tmp = VERTEX;
	tmp.x = VERTEX.x * twist_cos - VERTEX.y * twist_sin;
	tmp.y = VERTEX.x * twist_sin + VERTEX.y * twist_cos;
	VERTEX = tmp;
	normrot *= mat3(
			vec3(twist_cos,		-twist_sin,	0.0), 
			vec3(twist_sin,		twist_cos,	0.0), 
			vec3(0.0,			0.0,		1.0) );
	
	// S shape
	float s_a = ctrl_color.g * swim_angl * s_factor;
	VERTEX.x += s_a;
	
		// yaw
	float yaw_a = ctrl_color.b * swim_angl * yaw_factor;
	float yaw_cos = cos( yaw_a );
	float yaw_sin = sin( yaw_a );
	tmp.x = VERTEX.x * yaw_cos - VERTEX.z * yaw_sin;
	tmp.z = VERTEX.x * yaw_sin + VERTEX.z * yaw_cos;
	VERTEX = tmp;
	normrot *= mat3(
		vec3(yaw_cos,	0.0,	yaw_sin), 
		vec3(0.0,		1.0,	0.0), 
		vec3(-yaw_sin,	0.0,	yaw_cos) );
	
	///////////////
	// CURVATURE //
	///////////////
	
	// avoiding divisions by 0
	if ( curvature.x != 0.0 && curve_size.x != 0.0 ) {
		// radius proportional to curvature
		float radius = curve_size.x / curvature.x;
		// size of the arc / radius => angle
		float angl = ( VERTEX.z - curve_offset.x ) / radius;
		float r = curve_accel.x + radius - VERTEX.x;
		float ac = cos( angl );
		float as = sin( angl );
		VERTEX.x = radius - ( ac * r ) + curve_accel.x;
		VERTEX.z = ( as * r ) + curve_offset.x;
		normrot *= mat3(
			vec3(ac,	0.0,	as), 
			vec3(0.0,	1.0,	0.0), 
			vec3(-as,	0.0,	ac) );
	}
	// avoiding divisions by 0
	if ( curvature.y != 0.0 && curve_size.y != 0.0 ) {
		// radius proportional to curvature
		float radius = curve_size.y / curvature.y;
		// size of the arc / radius => angle
		float angl = ( VERTEX.z - curve_offset.y ) / radius;
		float r = curve_accel.y + radius - VERTEX.y;
		float ac = cos( angl );
		float as = sin( angl );
		VERTEX.y = radius - ( ac * r ) + curve_accel.y;
		VERTEX.z = ( as * r ) + curve_offset.y;
		normrot *= mat3(
			vec3(1.0,	0.0,		0.0), 
			vec3(0.0,	ac,		as), 
			vec3(0.0,	-as,	ac) );
	}
	// avoiding divisions by 0
	if ( curvature.z != 0.0 && curve_size.z != 0.0 ) {
		// radius proportional to curvature
		float radius = curve_size.z / curvature.z;
		float angl = ( VERTEX.x + curve_offset.z ) / radius;
		// size of the arc / radius => angle
		float r = curve_accel.z + radius - VERTEX.z;
		float ac = cos( angl );
		float as = sin( angl );
		VERTEX.x = ( as * r ) + curve_offset.z;
		VERTEX.z = radius - ( ac * r ) + curve_accel.z;
		normrot *= mat3(
			vec3(ac,	0.0,	as), 
			vec3(0.0,	1.0,	0.0), 
			vec3(-as,	0.0,	ac) );
	}
	
	NORMAL *= normrot;
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
	NORMALMAP = texture(texture_normal,base_uv).rgb;
	NORMALMAP_DEPTH = normal_scale;
	AO = dot(texture(texture_ambient_occlusion,base_uv),ao_texture_channel);
	AO_LIGHT_AFFECT = ao_light_affect;
}
