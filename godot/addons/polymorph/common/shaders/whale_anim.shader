shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;

// whale params
// 
uniform float time_offset = 0.0;
uniform float frequency = 0.42;
uniform float amplitude = 0.02;
uniform float zmult = 6.0;

uniform float x_phase_shift = 0.0;
uniform float xmin = 0.0;
uniform float xmax = 1.0;
uniform sampler2D x_influence : hint_white;

uniform float zmin = -1.0;
uniform float zmax = 1.0;
uniform sampler2D z_influence : hint_white;

// standard params
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_normal : hint_normal;
uniform float normal_scale : hint_range(-16,16);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

void vertex() {
	vec3 new_pos = VERTEX;
	float xpc = max(0.0,min(1.0,(abs(VERTEX.x)-xmin)/(xmax-xmin)));
	float xmult = texture(x_influence,vec2(xpc,0.0)).r;
	float zpc = max(0.0,min(1.0,(VERTEX.z-zmin)/(zmax-zmin)));
	float vz = VERTEX.z * zmult;
	float ystretch = sin( ( ( TIME + time_offset ) * frequency + vz + x_phase_shift * xmult ) );
	new_pos.y += ystretch * amplitude * xmult * texture(z_influence,vec2(zpc,0.0)).r;
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	VERTEX = new_pos;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
	NORMALMAP = texture(texture_normal,base_uv).rgb;
	NORMALMAP_DEPTH = normal_scale;
}
