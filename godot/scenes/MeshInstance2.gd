extends MeshInstance



func _ready():

	var surfTool = SurfaceTool.new()
	var mesh = Mesh.new()
	var vert_array = Array()
	var uv_array = Array()
	var st = SurfaceTool.new()

	vert_array.push_back(Vector3(0,0,0))
	vert_array.push_back(Vector3(0,1,0))
	vert_array.push_back(Vector3(1,1,0))

	vert_array.push_back(Vector3(0,0,0))
	vert_array.push_back(Vector3(1,1,0))
	vert_array.push_back(Vector3(1,0,0))

	uv_array.push_back(Vector2(0,0))
	uv_array.push_back(Vector2(0,1))
	uv_array.push_back(Vector2(1,1))

	uv_array.push_back(Vector2(0,0))
	uv_array.push_back(Vector2(1,1))
	uv_array.push_back(Vector2(1,0))

	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	for i in range(6):
		st.add_uv(uv_array[i])
		st.add_vertex(vert_array[i])
   
	st.commit(mesh)
	self.set_mesh(mesh)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
