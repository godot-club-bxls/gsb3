extends Spatial

func _ready():
	$Label.visible = false

func _on_Area_body_entered(body):
	$Label.visible = true


func _on_Area_body_exited(body):
	$Label.visible = false
