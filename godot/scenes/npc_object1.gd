extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$MeshInstance.create_convex_collision()
	var mesh_static_body = $MeshInstance.get_child(0)
	mesh_static_body.bounce = 1
	mesh_static_body.constant_linear_velocity = Vector3(1, 1, 1)
	mesh_static_body.constant_angular_velocity = Vector3(1, 1, 1)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
