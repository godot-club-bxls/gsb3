extends RigidBody

var linear_velocity_request = Vector3(0.0, 0.0, 0.0)
var angular_velocity_request = Vector3(0.0, 0.0, 0.0)
var pitch_control_multiplier = 1.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func _process(delta):
	if Input.is_action_pressed("move_left"):
		angular_velocity_request.y = pow(Input.get_action_strength("move_left"), 2.0) * 3.0
	elif Input.is_action_pressed("move_right"):
		angular_velocity_request.y = pow(Input.get_action_strength("move_right"), 2.0) * -3.0
	else:
		angular_velocity_request.y = 0.0
	
	if Input.is_action_pressed("ui_left"):
		angular_velocity_request.z = pow(Input.get_action_strength("ui_left"), 2.0) * 3.0
	elif Input.is_action_pressed("ui_right"):
		angular_velocity_request.z = pow(Input.get_action_strength("ui_right"), 2.0) * -3.0
	else:
		angular_velocity_request.z = 0.0
	
	if Input.is_action_pressed("ui_up"):
		angular_velocity_request.x = pow(Input.get_action_strength("ui_up"), 2.0) * 3.0 * pitch_control_multiplier
	elif Input.is_action_pressed("ui_down"):
		angular_velocity_request.x = pow(Input.get_action_strength("ui_down"), 2.0) * -3.0 * pitch_control_multiplier
	else:
		angular_velocity_request.x = 0.0
	
	if Input.is_action_pressed("move_forward"):
		linear_velocity_request.z = Input.get_action_strength("move_forward") * -5.0
	elif Input.is_action_pressed("move_backward"):
		linear_velocity_request.z = Input.get_action_strength("move_backward") * 5.0
	else:
		linear_velocity_request.z = 0.0
	
	if Input.is_action_pressed("-"):
		linear_velocity_request.y = Input.get_action_strength("player_move_up") * 5.0
	elif Input.is_action_pressed("-"):
		linear_velocity_request.y = Input.get_action_strength("player_move_down") * -5.0
	else:
		linear_velocity_request.y = 0.0
	
	if Input.is_action_pressed("-"):
		linear_velocity_request.x = Input.get_action_strength("player_move_left") * -5.0
	elif Input.is_action_pressed("-"):
		linear_velocity_request.x = Input.get_action_strength("player_move_right") * 5.0
	else:
		linear_velocity_request.x = 0.0
		
func _integrate_forces(var state):
	
	# Apply force to reach requested linear velocity
	var request = get_global_transform().basis * linear_velocity_request
	state.add_central_force(Vector3((request.x - state.linear_velocity.x) * 50.0, (request.y - state.linear_velocity.y) * 50.0, (request.z - state.linear_velocity.z) * 50.0))

	# Apply torque to reach requested angular velocity
	request = get_global_transform().origin * angular_velocity_request
	state.add_torque(Vector3((request.x - state.angular_velocity.x) * 10.0, (request.y - state.angular_velocity.y) * 10.0, (request.z - state.angular_velocity.z) * 10.0))
